﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TimeLimit : MonoBehaviour
{
    public string levelToLoad;
    public float timer = 30f;
    public Text timerSeconds;

    public GameObject _gameOverMenu;

    // Start is called before the first frame update
    void Start()
    {
        timerSeconds = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        timerSeconds.text = timer.ToString("f3");
        if (timer <= 0)
        {
            _gameOverMenu.SetActive(true);
        }
        if (timer <= 5)
        {
            timerSeconds.color = Color.red;
        }
    }
}
