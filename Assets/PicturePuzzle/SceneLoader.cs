﻿using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class SceneLoader : MonoBehaviour
{
    public GameObject _completeStep; 
    public TextMeshProUGUI _score;
    public TextMeshProUGUI _unlockLevel;
    public TextMeshProUGUI _unlockStep;

    public int _level;
    public int _step;

    [Header("Week 1 - Data")]
    public List<int> _languageScoreWeek1;
    private int _week1Day;

    [Header("Week 2 - Data")]
    public List<int> _languageScoreWeek2;
    private int _week2Day;

    [Header("Week 3 - Data")]
    public List<int> _languageScoreWeek3;
    private int _week3Day;

    private void Start()
    {
        if (PlayerPrefs.HasKey("Week1Day"))
        {
            print("has");
            _languageScoreWeek1.Clear();
            _week1Day = PlayerPrefs.GetInt("Week1Day");
            print("has 3" + _week1Day);

            for (int i = 0; i < _week1Day; i++)
            {
                print("has 2");
                _languageScoreWeek1.Add(PlayerPrefs.GetInt("Language_Score_Week1" + i));
            }
        }
        if (PlayerPrefs.HasKey("Week2Day"))
        {
            print("has");
            _languageScoreWeek2.Clear();
            _week1Day = PlayerPrefs.GetInt("Week1Day");
            print("has 3" + _week1Day);

            for (int i = 0; i < _week1Day; i++)
            {
                print("has 2");
                _languageScoreWeek2.Add(PlayerPrefs.GetInt("Language_Score_Week2" + i));
            }
        }
        if (PlayerPrefs.HasKey("Week3Day"))
        {
            print("has");
            _languageScoreWeek3.Clear();
            _week1Day = PlayerPrefs.GetInt("Week1Day");
            print("has 3" + _week1Day);

            for (int i = 0; i < _week1Day; i++)
            {
                print("has 2");
                _languageScoreWeek3.Add(PlayerPrefs.GetInt("Language_Score_Week3" + i));
            }
        }
    }

    public void LoadScene(string level)
    {
        SceneManager.LoadScene(level);
    }

    public void OnCompleteStep()
    {
        _completeStep.SetActive(true);

        _level = PlayerPrefs.GetInt("Language_Level");
        _step = PlayerPrefs.GetInt("Language_Step");

        #region Update player data.

        if (_level == 1)
        {
            _languageScoreWeek1.Add(PlayerPrefs.GetInt("LanguageScore"));

            if (PlayerPrefs.HasKey("Language_Score_Week1"))
            {
                PlayerPrefs.DeleteKey("Language_Score_Week1");
            }

            PlayerPrefs.SetInt("Week1Day", _languageScoreWeek1.Count);

            for (int i = 0; i < _languageScoreWeek1.Count; i++)
            {
                PlayerPrefs.SetInt("Language_Score_Week1" + i, _languageScoreWeek1[i]);
            }
        }

        if (_level == 2)
        {
            _languageScoreWeek2.Add(PlayerPrefs.GetInt("LanguageScore"));

            if (PlayerPrefs.HasKey("Language_Score_Week2"))
            {
                PlayerPrefs.DeleteKey("Language_Score_Week2");
            }

            PlayerPrefs.SetInt("Week2Day", _languageScoreWeek2.Count);

            for (int i = 0; i < _languageScoreWeek2.Count; i++)
            {
                PlayerPrefs.SetInt("Language_Score_Week2" + i, _languageScoreWeek2[i]);
            }
        }

        if (_level == 3)
        {
            _languageScoreWeek3.Add(PlayerPrefs.GetInt("LanguageScore"));

            if (PlayerPrefs.HasKey("Language_Score_Week3"))
            {
                PlayerPrefs.DeleteKey("Language_Score_Week3");
            }

            PlayerPrefs.SetInt("Week3Day", _languageScoreWeek3.Count);

            for (int i = 0; i < _languageScoreWeek3.Count; i++)
            {
                PlayerPrefs.SetInt("Language_Score_Week3" + i, _languageScoreWeek3[i]);
            }
        }

        #endregion

        if (_level == 3 && _step == 7)
        {
            // Language Game Over
        }
        else if (_level == 1)
        {
            if (_step < 14)
            {
                _step++;
            }
            else if (_step == 14)
            {
                _level = 2;
                _step = 1;
            }
        }
        else
        {
            if (_step < 7)
            {
                _step++;
            }
            else if (_step == 7)
            {
                _level++;
                _step = 1;
            }
        }

        _unlockStep.text = _step.ToString();
        _unlockLevel.text = _level.ToString();
        _score.text = PlayerPrefs.GetInt("LanguageScore").ToString();

        PlayerPrefs.SetInt("Language_Level",_level);
        PlayerPrefs.SetInt("Language_Step",_step);
        PlayerPrefs.SetInt("LanguageScore",0);
    }
}
