﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class ButtonClick : MonoBehaviour
{

    public string levelToLoad;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void NavigateToPage()
    {
        switch (gameObject.tag)
        {
            case "English":  // If the menu selection is English this will redirect to to Starting page of the game in English 
                //SceneManager.LoadScene("Estarted");
                Application.LoadLevel(levelToLoad);
                break;
            case "Sinhala":  // If the menu selection is Sinhala this will redirect to Starting page of the game in Sinhala
                //SceneManager.LoadScene("Sstarted"); // put scene name which should navigate when press this
                Application.LoadLevel(levelToLoad);
                break;
            case "Estarted": // Get Started with English
                //SceneManager.LoadScene("EP1");
                Application.LoadLevel(levelToLoad);
                break;
            case "Sstarted": // Get Started with Sinhala
                //SceneManager.LoadScene("SP1");
                Application.LoadLevel(levelToLoad);
                break;
            case "L2Estarted": // Get Started with English
                //SceneManager.LoadScene("L2EP1");
                Application.LoadLevel(levelToLoad);
                break;
            default: break;
        }
    }
}
