﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using System;

public class GameManager : MonoBehaviour
{
    [Header("Game UI Items")]
    public GameObject _pauseMenu;
    public GameObject _pauseBtn;
    public TextMeshProUGUI _dispalyCountDown;

    [Header("Game UI Items - Game Over Menu")]
    public TextMeshProUGUI _completedLevelNumber;
    public TextMeshProUGUI _completedStepNumber;
    public TextMeshProUGUI _correct;
    public TextMeshProUGUI _incorrect;
    public TextMeshProUGUI _missed;

    [Header("Game States")]
    public bool _isPauseGame = false;

    [Header("Count Down")]
    // Count down start value.
    [SerializeField] private int _timerStart = 3;

    [Header("Daily Progress")]
    public GameObject _englishPage;
    public GameObject _sinhalaPage;
    public Animator _menuAnim;
    public GameObject _dailyProgressDisplay;
    public Image _attentionProgressUI;
    public Image _concentrationProgressUI;

    [Header("Cut seens")]
    public GameObject _cutSeenScreen;
    public GameObject _gameOver_Menu;
    public GameObject _unlocktypeScreen;
    public GameObject _userGuideUI;
    public TextMeshProUGUI _cutSeenTitle;
    public Animator _cutSeenAnim;
    public Image _unlockItemImage;
    public Sprite _water;
    public Sprite _air;
    public TextMeshProUGUI _title;

    [Header("Achievements")]
    public GameObject _AchieveDisplay;
    public GameObject _showLevel;
    public GameObject _endGame1;
    public TextMeshProUGUI _nextLevel;
    public TextMeshProUGUI _nextStep;

    private float _speed;

    public static GameManager Instance;

    private void Start()
    {
        _isPauseGame = true;
        _dispalyCountDown.text = "";
        print(MissionController.Instance._currentStepCount);

        //if (PlayerData.Instance._isFirstTimeStart)
        //{
        //    //StartCoroutine(SetCutSeen("Start"));
        //    //_userGuideUI.SetActive(true);
        //}
        //else 
        if (PlayerPrefs.GetInt("Level_Number") == 2 && PlayerPrefs.GetInt("Step_Number") == 1)
        {
            StartCoroutine(SetCutSeen("Unlock Item"));
            _userGuideUI.SetActive(false);
            _gameOver_Menu.SetActive(false);
            _unlocktypeScreen.SetActive(true);
            _unlockItemImage.sprite = _water;
        }
        else if (PlayerPrefs.GetInt("Level_Number") == 3 && PlayerPrefs.GetInt("Step_Number") == 1)
        {
            StartCoroutine(SetCutSeen("Unlock Item"));
            _userGuideUI.SetActive(false);
            _gameOver_Menu.SetActive(false);
            _unlocktypeScreen.SetActive(true);
            _unlockItemImage.sprite = _air;
        }
        else
        {
            // Level 4
            // No cut seen 
            StartCoroutine(StartCountDown()); // Start count down timer.
            _cutSeenTitle.text = "";
        }
    }

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    public void OnClickPauseMenu()
    {
        SoundManager.Instance.PlayButtonClickSound();
        if (!_isPauseGame)
        {
            _pauseMenu.SetActive(true);
            _isPauseGame = true;
        }
    }

    public void OnClickBackToMenu()
    {
        SoundManager.Instance.PlayButtonClickSound();
        SceneManager.LoadScene(3);
    }

    public void OnClickPlayGame()
    {
        SoundManager.Instance.PlayButtonClickSound();
        _pauseMenu.SetActive(false);
        StartCoroutine(StartCountDown());
    }

    public void OnClickDailyProgress_Btn()
    {
        SoundManager.Instance.PlayButtonClickSound();
        _dailyProgressDisplay.SetActive(true);
        _AchieveDisplay.SetActive(false);
        // Play Animation.
        _menuAnim.SetBool("Show", true);
        _title.text = "Daily Progress";

        // Display Attention.
        _attentionProgressUI.fillAmount = ScoreManager.Instance._attentionProgress;

        // Display Concentration.
        _concentrationProgressUI.fillAmount = ScoreManager.Instance._concentrationProgress;
    }

    public void OnClickBackBtn()
    {
        SoundManager.Instance.PlayButtonClickSound();
        SoundManager.Instance.PlayButtonClickSound();
        _menuAnim.SetBool("Show", false);
    }

    public void OnClickGotItBtn()
    {
        SoundManager.Instance.PlayButtonClickSound();
        SoundManager.Instance.PlayButtonClickSound();
        _userGuideUI.SetActive(false);
        _unlocktypeScreen.SetActive(false);
        _cutSeenAnim.SetBool("PopUp", false);
        _cutSeenTitle.text = "";
        //_cutSeenScreen.SetActive(false);
        StartCoroutine(StartCountDown()); // Start count down timer.
    }

    // Count down timer.
    public IEnumerator StartCountDown()
    {
        _dispalyCountDown.enabled = true;

        for (int i = _timerStart; i >= 0; i--)
        {
            if(i != 0)
            {
                _dispalyCountDown.text = i.ToString();
            }
            else
            {
                _dispalyCountDown.text = "GO!";
            }
            yield return new WaitForSeconds(1.0f);

            if(i == 0)
            {
                _isPauseGame = false;
                _dispalyCountDown.enabled = false;
                MissionController.Instance.SetLevelVehicleTypes(MissionController.Instance._level);
                SoundManager.Instance._backGround.Play();
            }
        }
    }

    // Game over.
    public void GameOver()
    {
        SoundManager.Instance._backGround.Stop();
        SoundManager.Instance.GameOverSound();
        _userGuideUI.SetActive(false);
        _unlocktypeScreen.SetActive(false);
        _gameOver_Menu.SetActive(true);
        _pauseBtn.SetActive(false);
        StartCoroutine(SetCutSeen("Game Over"));

        // Set UI items.
        _completedLevelNumber.text = "Level " + MissionController.Instance._previousLevelIndex;
        _completedStepNumber.text = "Step " + MissionController.Instance._previousStep;
        //int totalScore = (int)ScoreManager.Instance._gameTime;
        //_score.text = totalScore.ToString();
        _correct.text = ScoreManager.Instance._correctTriesCount.ToString();
        _incorrect.text = ScoreManager.Instance._incorrectCount.ToString();
        _missed.text = ScoreManager.Instance._missedCount.ToString();

        ScoreManager.Instance.UpdatePlayerData();
        // Update speed Without ML
        if (ScoreManager.Instance._attentionProgress < 0.5f)
        {
            _speed = VehicleGenerator.Instance._speed - ((MissionController.Instance._speedAccelator / 2) - MissionController.Instance._speedAccelator * ScoreManager.Instance._attentionProgress);

            if(_speed < VehicleGenerator.Instance._minSpeed)
            {
                _speed = VehicleGenerator.Instance._minSpeed;
            }
        }
        else
        {
            _speed = VehicleGenerator.Instance._speed + (MissionController.Instance._speedAccelator * ScoreManager.Instance._attentionProgress);
        }
        GameData.Instance.updateGameData(_speed, (MissionController.Instance._requiredVehicleCount + MissionController.Instance._vehicleCountIncrese), DateTime.Now);


        // With ML
        //GameData.Instance.updateGameData((VehicleGenerator.Instance._speed + (GameData.Instance.chooseDifficulty(GameData.Instance.qlearning()))), (MissionController.Instance._requiredVehicleCount + MissionController.Instance._vehicleCountIncrese), DateTime.Now);
    }

    public IEnumerator SetCutSeen(string title)
    {
        _isPauseGame = true;
        _cutSeenAnim.SetBool("Title", true);
        _cutSeenTitle.text = title;

        yield return new WaitForSeconds(2.0f);
        _cutSeenAnim.SetBool("PopUp", true);
    }

    public void OnClickAchievementsBtn()
    {
        SoundManager.Instance.PlayButtonClickSound();
        _AchieveDisplay.SetActive(true);
        _dailyProgressDisplay.SetActive(false);
        // Play Animation.
        _menuAnim.SetBool("Show", true);
        _title.text = "Achievement";

        if (PlayerPrefs.GetString("Game_1_Over") == "True")
        {
            _showLevel.SetActive(false);
            _endGame1.SetActive(true);
        }
        else
        {
            _showLevel.SetActive(true);
            _endGame1.SetActive(false);
            _nextLevel.text = "Level " + PlayerPrefs.GetInt("Level_Number").ToString();
            _nextStep.text = "Step " + PlayerPrefs.GetInt("Step_Number").ToString();
        }
    }

    public void OnClickSwitchLanguage()
    {
        SoundManager.Instance.PlayButtonClickSound();
        if (_sinhalaPage.activeInHierarchy)
        {
            _englishPage.SetActive(true);
            _sinhalaPage.SetActive(false);
        }
        else if (_englishPage.activeInHierarchy)
        {
            _sinhalaPage.SetActive(true);
            _englishPage.SetActive(false);
        }
    }
}
