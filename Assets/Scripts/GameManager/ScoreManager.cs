﻿using TMPro;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScoreManager : MonoBehaviour
{
    [Header("UI Texts")]
    public TextMeshProUGUI _totalScoreUI;
    public TextMeshProUGUI _correctTriesUI;
    public TextMeshProUGUI _missedTriesUI;
    public TextMeshProUGUI _incorrectTriesUI;
    public TextMeshProUGUI _requiredVehicleCountUI;
    public GameObject _wrongDragNotification;

    [Header("Player stats in the step")]
    public int _correctTriesCount = 0;
    public int _missedCount = 0;
    public int _incorrectCount = 0;
    public float _gameTime = 0;

    [Header("Week 1 - Data")]
    public List<float> _attentionWeek1;
    public List<float> _concentrationWeek1;
    private int _week1Day;

    [Header("Week 2 - Data")]
    public List<float> _attentionWeek2;
    public List<float> _concentrationWeek2;
    private int _week2Day;

    [Header("Week 3 - Data")]
    public List<float> _attentionWeek3;
    public List<float> _concentrationWeek3;
    private int _week3Day;

    [Header("Week 4 - Data")]
    public List<float> _attentionWeek4;
    public List<float> _concentrationWeek4;
    private int _week4Day;

    [Header("Concentration Time")]
    public bool _isConcentration = true;
    public float _concentrationTime = 0.0f;

    [Header("Current Level Attention And Concentration")]
    public float _attentionProgress;
    public float _concentrationProgress;

    public static ScoreManager Instance;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        if(PlayerPrefs.GetInt("Vehicle_Count") == 0)
        {
            _requiredVehicleCountUI.text = MissionController.Instance._requiredVehicleCount.ToString();
        }
        else
        {
            _requiredVehicleCountUI.text = PlayerPrefs.GetInt("Vehicle_Count").ToString();
        }

        if (PlayerPrefs.HasKey("Week1Day"))
        {
            print("has");
            _attentionWeek1.Clear();
            _concentrationWeek1.Clear();
            _week1Day = PlayerPrefs.GetInt("Week1Day");
            print("has 3" + _week1Day);

            for (int i = 0; i < _week1Day; i++)
            {
                print("has 2");
                _attentionWeek1.Add(PlayerPrefs.GetFloat("AttentionWeek1" + i));
                _concentrationWeek1.Add(PlayerPrefs.GetFloat("ConcentrationWeek1" + i));
            }
        }
        if (PlayerPrefs.HasKey("Week2Day"))
        {
            _attentionWeek2.Clear();
            _concentrationWeek2.Clear();
            _week2Day = PlayerPrefs.GetInt("Week2Day");

            for (int i = 0; i < _week2Day; i++)
            {
                _attentionWeek2.Add(PlayerPrefs.GetFloat("AttentionWeek2" + i));
                _concentrationWeek2.Add(PlayerPrefs.GetFloat("ConcentrationWeek2" + i));
            }
        }
        if (PlayerPrefs.HasKey("Week3Day"))
        {
            _attentionWeek3.Clear();
            _concentrationWeek3.Clear();
            _week3Day = PlayerPrefs.GetInt("Week3Day");

            for (int i = 0; i < _week3Day; i++)
            {
                _attentionWeek3.Add(PlayerPrefs.GetFloat("AttentionWeek3" + i));
                _concentrationWeek3.Add(PlayerPrefs.GetFloat("ConcentrationWeek3" + i));
            }
        }
        if (PlayerPrefs.HasKey("Week4Day"))
        {
            _attentionWeek4.Clear();
            _concentrationWeek4.Clear();
            _week4Day = PlayerPrefs.GetInt("Week4Day");

            for (int i = 0; i < _week4Day; i++)
            {
                _attentionWeek4.Add(PlayerPrefs.GetFloat("AttentionWeek4" + i));
                _concentrationWeek4.Add(PlayerPrefs.GetFloat("ConcentrationWeek4" + i));
            }
        }
    }

    private void Update()
    {
        // Count down timer.
        if (!GameManager.Instance._isPauseGame)
        {
            _gameTime += Time.deltaTime;
            //int score = (int)_gameTime;
            //_totalScoreUI.text = score.ToString();

            if (_isConcentration)
            {
                _concentrationTime += Time.deltaTime;
            }
        }
    }

    public void LeftSwipe(string _tag)
    {
        if(_tag != "Water")
        {
            UpdateIncorrectCount_Tries();
        }
        else
        {
            // Add Score
            UpdateCorrectTries_Count();
        }
    }

    public void RightSwipe(string _tag)
    {
        if (_tag != "Air")
        {
            
            UpdateIncorrectCount_Tries();
        }
        else
        {
            // Add Score
            UpdateCorrectTries_Count();
        }
    }

    public void LeftBottom(string _tag)
    {
        if (_tag != "Land_2_Wheel")
        {
            UpdateIncorrectCount_Tries();
        }
        else
        {
            // Add Score
            UpdateCorrectTries_Count();
        }
    }

    public void RightBottom(string _tag)
    {
        if (_tag != "Land_4_Wheel")
        {
            UpdateIncorrectCount_Tries();
        }
        else
        {
            // Add Score
            UpdateCorrectTries_Count();
        }
    }

    public void UpdateCorrectTries_Count()
    {
        SoundManager.Instance.CorrectDragSound();
        _isConcentration = true;
        _correctTriesCount += 1;
        _correctTriesUI.text = _correctTriesCount.ToString();
        _requiredVehicleCountUI.text = (MissionController.Instance._requiredVehicleCount - _correctTriesCount).ToString();


        if (_correctTriesCount == MissionController.Instance._requiredVehicleCount)
        {
            MissionController.Instance.ChangeMission(); // Set next mission.

            // Update playe pref.
            SendPlayerStat();
        }
    }

    public void UpdateMissedTries_Count()
    {
        _isConcentration = false;
        _missedCount += 1;
        _missedTriesUI.text = _missedCount.ToString();
        SoundManager.Instance.MissedSound();
    }

    public void UpdateIncorrectCount_Tries()
    {
        SoundManager.Instance.WrongDragSound();
        _isConcentration = false;
        _incorrectCount += 1;
        _incorrectTriesUI.text = _incorrectCount.ToString();
        StartCoroutine(SetWrongDragNotification());
    }

    public void SendPlayerStat()
    {
        GameData.Instance.UpdatePlayerStat(_correctTriesCount, _incorrectCount, _missedCount);
    }

    public IEnumerator SetWrongDragNotification()
    {
        _wrongDragNotification.SetActive(true);

        yield return new WaitForSeconds(0.5f);

        _wrongDragNotification.SetActive(false);
    }

    public void UpdatePlayerData()
    {
        print(_correctTriesCount);
        print(_missedCount);
        print(_incorrectCount);
        print(MissionController.Instance._maxVehicleCount);
        print(_correctTriesCount + (((_missedCount + _incorrectCount) / _correctTriesCount) * MissionController.Instance._maxVehicleCount));
        print(_correctTriesCount + ((_missedCount + _incorrectCount) / _correctTriesCount * MissionController.Instance._maxVehicleCount));
        _attentionProgress = (float)_correctTriesCount / (_correctTriesCount + (((float)(_missedCount + _incorrectCount) / _correctTriesCount) * MissionController.Instance._maxVehicleCount));
        _concentrationProgress = _concentrationTime / _gameTime;

        if (MissionController.Instance._levelIndex == 1)
        {
            _attentionWeek1.Add(_attentionProgress);
            _concentrationWeek1.Add(_concentrationProgress);

            if (PlayerPrefs.HasKey("AttentionWeek1"))
            {
                PlayerPrefs.DeleteKey("AttentionWeek1");
                PlayerPrefs.DeleteKey("ConcentrationWeek1");
            }

            PlayerPrefs.SetInt("Week1Day", _attentionWeek1.Count);

            for (int i = 0; i < _attentionWeek1.Count; i++)
            {
                print("12345");
                PlayerPrefs.SetFloat("AttentionWeek1" + i, _attentionWeek1[i]);
                PlayerPrefs.SetFloat("ConcentrationWeek1" + i, _concentrationWeek1[i]);
            }

            PlayerAuthentication.Instance.UpdateUserProgressData((int)(_attentionWeek1[_attentionWeek1.Count - 1] * 100), (int)(_concentrationWeek1[_concentrationWeek1.Count - 1] * 100));
        }

        if (MissionController.Instance._levelIndex == 2)
        {
            _attentionWeek2.Add(_attentionProgress);
            _concentrationWeek2.Add(_concentrationProgress);

            if (PlayerPrefs.HasKey("AttentionWeek2"))
            {
                PlayerPrefs.DeleteKey("AttentionWeek2");
                PlayerPrefs.DeleteKey("ConcentrationWeek2");
            }

            PlayerPrefs.SetInt("Week2Day", _attentionWeek2.Count);

            for (int i = 0; i < _attentionWeek2.Count; i++)
            {
                PlayerPrefs.SetFloat("AttentionWeek2" + i, _attentionWeek2[i]);
                PlayerPrefs.SetFloat("ConcentrationWeek2" + i, _concentrationWeek2[i]);
            }

            PlayerAuthentication.Instance.UpdateUserProgressData((int)(_attentionWeek2[_attentionWeek2.Count - 1] * 100), (int)(_concentrationWeek2[_concentrationWeek2.Count - 1] * 100));
        }

        if (MissionController.Instance._levelIndex == 3)
        {
            _attentionWeek3.Add(_attentionProgress);
            _concentrationWeek3.Add(_concentrationProgress);

            if (PlayerPrefs.HasKey("AttentionWeek3"))
            {
                PlayerPrefs.DeleteKey("AttentionWeek3");
                PlayerPrefs.DeleteKey("ConcentrationWeek3");
            }

            PlayerPrefs.SetInt("Week3Day", _attentionWeek3.Count);

            for (int i = 0; i < _attentionWeek3.Count; i++)
            {
                PlayerPrefs.SetFloat("AttentionWeek3" + i, _attentionWeek3[i]);
                PlayerPrefs.SetFloat("ConcentrationWeek3" + i, _concentrationWeek3[i]);
            }

            PlayerAuthentication.Instance.UpdateUserProgressData((int)(_attentionWeek3[_attentionWeek3.Count - 1] * 100), (int)(_concentrationWeek3[_concentrationWeek3.Count - 1] * 100));
        }

        if (MissionController.Instance._levelIndex == 4)
        {
            _attentionWeek4.Add(_attentionProgress);
            _concentrationWeek4.Add(_concentrationProgress);

            if (PlayerPrefs.HasKey("AttentionWeek4"))
            {
                PlayerPrefs.DeleteKey("AttentionWeek4");
                PlayerPrefs.DeleteKey("ConcentrationWeek4");
            }

            PlayerPrefs.SetInt("Week4Day", _attentionWeek3.Count);

            for (int i = 0; i < _attentionWeek4.Count; i++)
            {
                PlayerPrefs.SetFloat("AttentionWeek4" + i, _attentionWeek4[i]);
                PlayerPrefs.SetFloat("ConcentrationWeek4" + i, _concentrationWeek4[i]);
            }

            PlayerAuthentication.Instance.UpdateUserProgressData((int)(_attentionWeek4[_attentionWeek4.Count - 1] * 100), (int)(_concentrationWeek4[_concentrationWeek4.Count - 1] * 100));
        }
    }
}
