﻿using UnityEngine;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

public class SendMail : MonoBehaviour
{
    public static SendMail Instance;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    public void SendEmail()
    {
        MailMessage mail = new MailMessage();
        SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
        SmtpServer.Timeout = 10000;
        SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
        SmtpServer.UseDefaultCredentials = false;
        SmtpServer.Port = 587;

        mail.From = new MailAddress("careumed2020@gmail.com");
        mail.To.Add(new MailAddress(PlayerPrefs.GetString("DoctorMail")));
        mail.CC.Add(new MailAddress(PlayerPrefs.GetString("CareGiverMail")));

        mail.Subject = "Week " + PlayerPrefs.GetInt("Level_Number") + " report of " + PlayerPrefs.GetString("PatientName");
        // Body start.
        mail.Body = "Patient Name   : " + PlayerPrefs.GetString("PatientName") + "\n" +
                    "Patient Age    : " + PlayerPrefs.GetString("PatientAge") + "\n" +
                    "Patient Mail   : " + PlayerPrefs.GetString("PatientMail") + "\n" +
                    "CareGiver Name : " + PlayerPrefs.GetString("CareGiverName") + "\n" +
                    "CareGiver Mail : " + PlayerPrefs.GetString("CareGiverMail") + "\n" + "\n" + "\n" +
                    "Attention And Concentration" + "\n" + "\n";

        if(PlayerPrefs.GetInt("Level_Number") == 1)
        {
            for (int i = 0; i < PlayerPrefs.GetInt("Week1Day"); i++)
            {
                mail.Body += "Day " + (i + 1).ToString() + " => " + "Attention " + ((int)(PlayerPrefs.GetFloat("AttentionWeek1" + i) * 100)).ToString() + "%" +" , "+ "Concentration " + ((int)(PlayerPrefs.GetFloat("ConcentrationWeek1" + i) * 100)).ToString() + "%" + "\n";
            }
        }
        if (PlayerPrefs.GetInt("Level_Number") == 2)
        {
            for (int i = 0; i < PlayerPrefs.GetInt("Week2Day"); i++)
            {
                mail.Body += "Day " + (i + 1).ToString() + " => " + "Attention = " + ((int)(PlayerPrefs.GetFloat("AttentionWeek2" + i) * 100)).ToString() + "%" + " , " + "Concentration = " + ((int)(PlayerPrefs.GetFloat("ConcentrationWeek2" + i) * 100)).ToString() + "%" + "\n";
            }
        }
        if (PlayerPrefs.GetInt("Level_Number") == 3)
        {
            for (int i = 0; i < PlayerPrefs.GetInt("Week3Day"); i++)
            {
                mail.Body += "Day " + (i + 1).ToString() + " => " + "Attention = " + ((int)(PlayerPrefs.GetFloat("AttentionWeek3" + i) * 100)).ToString() + "%" + " , " + "Concentration = " + ((int)(PlayerPrefs.GetFloat("ConcentrationWeek3" + i) * 100)).ToString() + "%" + "\n";
            }
        }
        if (PlayerPrefs.GetInt("Level_Number") == 4)
        {
            for (int i = 0; i < PlayerPrefs.GetInt("Week4Day"); i++)
            {
                mail.Body += "Day " + (i + 1).ToString() + " => " + "Attention = " + ((int)(PlayerPrefs.GetFloat("AttentionWeek4" + i) * 100)).ToString() + "%" + " , " + "Concentration = " + ((int)(PlayerPrefs.GetFloat("ConcentrationWeek4" + i) * 100)).ToString() + "%" + "\n";
            }
        }
        // Body end.

        SmtpServer.Credentials = new System.Net.NetworkCredential("careumed2020@gmail.com", "careu2020") as ICredentialsByHost; SmtpServer.EnableSsl = true;
        ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        };

        mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess;
        SmtpServer.Send(mail);
    }
}
