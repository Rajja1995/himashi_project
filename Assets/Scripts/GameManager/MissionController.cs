﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public enum Levels
{
    LEVEL_1,
    LEVEL_2,
    LEVEL_3,
    LEVEL_4
}

public class MissionController : MonoBehaviour
{
    [Header("UI Objects")]
    public GameObject _land_2_Wheels_UI;
    public GameObject _land_4_Wheels_UI;
    public GameObject _water_UI;
    public GameObject _air_UI;

    [Header("Step UP data")]
    public float _speedAccelator = 0.2f;
    public int _vehicleCountIncrese = 1;

    // Activate Vehicle types count.
    public int _activateVehicleTypes = 2;

    // Minimum correct tries need to complete the current step.
    public int _requiredVehicleCount = 1;
    public int _maxVehicleCount;

    [Header("Steps in a level")]
    // Steps in current level
    public int _currentStepCount = 1;
    public int _previousStep = 1;
    [SerializeField] private int _maxStepsInLevel = 3;

    [Header("Levels")]
    public Levels _level;
    public int _levelIndex = 1; // index = 1 -> Level 1 , index = 2 -> Level 2 , index = 3 -> Level 3 
    public int _previousLevelIndex = 1;

    [Header("Send Mail")]
    public GameObject _sendingMailPage;

    public DateTime _lastPlayDate;

    public static MissionController Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        _lastPlayDate = DateTime.Now;

        // If player is a beginner game starts with level 1.
        if (GameData.Instance._isFirstTimeStart)
        {
            PlayerPrefs.SetString("Game_1_Over", "False");
            _maxVehicleCount = _requiredVehicleCount;
            for (int i = 0; i < 28; i++)
            {
                _maxVehicleCount += _vehicleCountIncrese;
            }
            PlayerPrefs.SetInt("Max_Vehicle_Count", _maxVehicleCount);

            _level = Levels.LEVEL_1;
            _levelIndex = 1;
            _currentStepCount = 1;

            PlayerPrefs.SetInt("Step_Number", _currentStepCount); 
            PlayerPrefs.SetInt("Level_Number", _levelIndex);
            PlayerPrefs.SetInt("Vehicle_Count",_requiredVehicleCount);
        }
        else
        {
            _maxVehicleCount = PlayerPrefs.GetInt("Max_Vehicle_Count");
            // Check player current level form player prefs.
            _levelIndex = PlayerPrefs.GetInt("Level_Number");
            _currentStepCount = PlayerPrefs.GetInt("Step_Number");
            _requiredVehicleCount = PlayerPrefs.GetInt("Vehicle_Count");

            if (_levelIndex == 1)
            {
                _level = Levels.LEVEL_1;
                _activateVehicleTypes = 2;
            }
            if (_levelIndex == 2)
            {
                _level = Levels.LEVEL_2;
                _activateVehicleTypes = 3;
            }
            if (_levelIndex == 3)
            {
                _level = Levels.LEVEL_3;
                _activateVehicleTypes = 4;
            }
            if (_levelIndex == 4)
            {
                _level = Levels.LEVEL_4;
                _activateVehicleTypes = 4;
            }
        }
        // Set current level and 
    }

    public void SetLevelVehicleTypes(Levels level)
    {
        if(_level == Levels.LEVEL_1)
        {
            _land_2_Wheels_UI.SetActive(true);
            _land_4_Wheels_UI.SetActive(true);
        }

        if (_level == Levels.LEVEL_2)
        {
            _land_2_Wheels_UI.SetActive(true);
            _land_4_Wheels_UI.SetActive(true);
            _water_UI.SetActive(true);
        }

        if (_level == Levels.LEVEL_3)
        {
            _land_2_Wheels_UI.SetActive(true);
            _land_4_Wheels_UI.SetActive(true);
            _water_UI.SetActive(true);
            _air_UI.SetActive(true);
        }

        if (_level == Levels.LEVEL_4)
        {
            _land_2_Wheels_UI.SetActive(true);
            _land_4_Wheels_UI.SetActive(true);
            _water_UI.SetActive(true);
            _air_UI.SetActive(true);
        }
    }

    public void ChangeMission()
    {
        // Game Over menu popup.
        _previousStep = _currentStepCount;
        _previousLevelIndex = _levelIndex;

        GameManager.Instance.GameOver();
        if (_currentStepCount == _maxStepsInLevel)
        {
            if(_level == Levels.LEVEL_4)
            {
                // Game Over.
                completeGame_1();
            }
            else
            {
                // Change Level.
                print("call");
                GameData.Instance.UpdateLevel();
                GameData.Instance.UpdateStep(1);
            }

            StartCoroutine(SendWeeklyProgress());
        }
        else
        {
            _currentStepCount++;
            GameData.Instance.UpdateStep(_currentStepCount);
        }
        
        // Game continue on next day
    }

    public void completeGame_1()
    {
        PlayerPrefs.SetString("Game_1_Over", "True");
    }

    public IEnumerator SendWeeklyProgress()
    {
        yield return new WaitForSeconds(5.0f);
        _sendingMailPage.SetActive(true);
        SendMail.Instance.SendEmail();

        yield return new WaitForSeconds(3.0f);
        _sendingMailPage.SetActive(false);

    }
}
