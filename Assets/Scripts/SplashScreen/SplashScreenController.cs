﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class SplashScreenController : MonoBehaviour
{
    [SerializeField] private string _caption = "We care Always...";
    public TextMeshProUGUI _captionDisplay;
    [SerializeField] private float _typingSpeed = 0.2f;
    public Animator _logoAnim;

    private void Start()
    {
        //PlayerPrefs.DeleteAll();
    }

    // Call by animator event.
    public void StartText()
    {
        StartCoroutine(TypeCaption());
    }

    // Type Caption.
    IEnumerator TypeCaption()
    {
        foreach (char letter in _caption.ToCharArray())
        {
            _captionDisplay.text += letter;
            yield return new WaitForSeconds(_typingSpeed);

            if(_captionDisplay.text == _caption)
            {
                SceneManager.LoadScene(1);
            }
        }
    }
}
