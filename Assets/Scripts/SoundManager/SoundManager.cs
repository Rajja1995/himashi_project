﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [SerializeField] private AudioSource _buttonClick;
    [SerializeField] private AudioSource _wrongDrag;
    [SerializeField] private AudioSource _missed;
    [SerializeField] private AudioSource _correct;
    [SerializeField] private AudioSource _gameOver;
    public AudioSource _backGround;

    public static SoundManager Instance;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    public void PlayButtonClickSound()
    {
        _buttonClick.Play();
    }

    public void WrongDragSound()
    {
        _wrongDrag.Play();
    }

    public void MissedSound()
    {
        _missed.Play();
    }

    public void CorrectDragSound()
    {
        _correct.Play();
    }

    public void GameOverSound()
    {
        _gameOver.Play();
    }

}
