﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManagerScript : MonoBehaviour
{
    public bool counterDownDone = false;

    public GameObject _gameOver;
    public TextMeshProUGUI _scoreText;
    public TextMeshProUGUI _correctCount;
    public string levelToLoad;

    private int levelNumber;
    private int stepNumber;

    [Header("Week 1 - Data")]
    public List<int> _score_EF_Week1;
    private int _week1Day;

    [Header("Week 2 - Data")]
    public List<int> _score_EF_Week2;
    private int _week2Day;

    [Header("Week 3 - Data")]
    public List<int> _score_EF_Week3;
    private int _week3Day;

    [Header("Week 4 - Data")]
    public List<int> _score_EF_Week4;
    private int _week4Day;

    private void Start()
    {
        levelNumber = PlayerPrefs.GetInt("EF_Level");
        stepNumber = PlayerPrefs.GetInt("EF_Step");

        if (PlayerPrefs.HasKey("Week1Day"))
        {
            _score_EF_Week1.Clear();
            _week1Day = PlayerPrefs.GetInt("Week1Day");

            for (int i = 0; i < _week1Day; i++)
            {
                _score_EF_Week1.Add(PlayerPrefs.GetInt("EF_ScoreWeek1" + i));
            }
        }
        if (PlayerPrefs.HasKey("Week2Day"))
        {
            _score_EF_Week2.Clear();
            _week2Day = PlayerPrefs.GetInt("Week2Day");

            for (int i = 0; i < _week2Day; i++)
            {
                _score_EF_Week2.Add(PlayerPrefs.GetInt("EF_ScoreWeek2" + i));
            }
        }
        if (PlayerPrefs.HasKey("Week3Day"))
        {
            _score_EF_Week3.Clear();
            _week3Day = PlayerPrefs.GetInt("Week3Day");

            for (int i = 0; i < _week3Day; i++)
            {
                _score_EF_Week3.Add(PlayerPrefs.GetInt("EF_ScoreWeek3" + i));
            }
        }
        if (PlayerPrefs.HasKey("Week4Day"))
        {
            _score_EF_Week4.Clear();
            _week4Day = PlayerPrefs.GetInt("Week4Day");

            for (int i = 0; i < _week4Day; i++)
            {
                _score_EF_Week4.Add(PlayerPrefs.GetInt("EF_ScoreWeek4" + i));
            }
        }
    }
    public void SetCutSeen()
    {
        UpdateScore();
        UpdateLevel();
        _gameOver.SetActive(true);
        _scoreText.text = ((int)PlayerPrefs.GetFloat("CurrentScore")).ToString();
        _correctCount.text = PlayerPrefs.GetInt("CurrentCorrectCount").ToString();
    }

    public void OnClickBackToMenu()
    {
        SceneManager.LoadScene(levelToLoad);
    }

    private void UpdateLevel()
    {
        if(stepNumber == 7)
        {
            if(levelNumber == 4)
            {
                // Main menu
            }
            else
            {
                levelNumber += 1;
                stepNumber = 1;
            }
        }
        else
        {
            stepNumber += 1;
        }

        PlayerPrefs.SetInt("EF_Level", levelNumber);
        PlayerPrefs.SetInt("EF_Step", stepNumber);
    }

    private void UpdateScore()
    {
        if (levelNumber == 1)
        {
            _score_EF_Week1.Add((int)PlayerPrefs.GetFloat("CurrentScore"));

            if (PlayerPrefs.HasKey("EF_ScoreWeek1"))
            {
                PlayerPrefs.DeleteKey("EF_ScoreWeek1");
            }

            PlayerPrefs.SetInt("Week1Day", _score_EF_Week1.Count);

            for (int i = 0; i < _score_EF_Week1.Count; i++)
            {
                PlayerPrefs.SetInt("EF_ScoreWeek1" + i, _score_EF_Week1[i]);
            }
        }

        if (levelNumber == 2)
        {
            _score_EF_Week2.Add((int)PlayerPrefs.GetFloat("CurrentScore"));

            if (PlayerPrefs.HasKey("EF_ScoreWeek2"))
            {
                PlayerPrefs.DeleteKey("EF_ScoreWeek2");
            }

            PlayerPrefs.SetInt("Week2Day", _score_EF_Week2.Count);

            for (int i = 0; i < _score_EF_Week2.Count; i++)
            {
                PlayerPrefs.SetInt("EF_ScoreWeek2" + i, _score_EF_Week2[i]);
            }
        }

        if (levelNumber == 3)
        {
            _score_EF_Week3.Add((int)PlayerPrefs.GetFloat("CurrentScore"));

            if (PlayerPrefs.HasKey("EF_ScoreWeek3"))
            {
                PlayerPrefs.DeleteKey("EF_ScoreWeek3");
            }

            PlayerPrefs.SetInt("Week3Day", _score_EF_Week3.Count);

            for (int i = 0; i < _score_EF_Week3.Count; i++)
            {
                PlayerPrefs.SetInt("EF_ScoreWeek3" + i, _score_EF_Week3[i]);
            }
        }

        if (levelNumber == 4)
        {
            _score_EF_Week4.Add((int)PlayerPrefs.GetFloat("CurrentScore"));

            if (PlayerPrefs.HasKey("EF_ScoreWeek4"))
            {
                PlayerPrefs.DeleteKey("EF_ScoreWeek4");
            }

            PlayerPrefs.SetInt("Week4Day", _score_EF_Week4.Count);

            for (int i = 0; i < _score_EF_Week4.Count; i++)
            {
                PlayerPrefs.SetInt("EF_ScoreWeek4" + i, _score_EF_Week4[i]);
            }
        }
    }
}
