﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleController : MonoBehaviour
{
    public Transform _CheckingPosition;
    public Transform _deletePosition;

    public bool _isDragging = false;
    public bool _inDragArea = false;
    public Transform _dragPosition;
    private float _lerpFrequency;

    public bool _passedPosition;

    private void Update()
    {
        if (!GameManager.Instance._isPauseGame && !_isDragging)
        {
            // Move Vehicle
            transform.position += new Vector3(0, -1, 0) * VehicleGenerator.Instance._speed * Time.deltaTime;

            if (!_passedPosition)
            {
                if (transform.position.y < 5)
                {
                    VehicleGenerator.Instance.SelectVehicleType();
                    _passedPosition = true;
                }
            }

            if (transform.position.y < -5.5f)
            {
                ScoreManager.Instance.UpdateMissedTries_Count();
                VehicleGenerator.Instance.DeleteVehicle();
            }
        }

        if (_isDragging)
        {
            DragVehicle();
        }
    }   

    public void SetDragPosition(Transform _dragPos,float _frequency)
    {
        _isDragging = true;
        _dragPosition = _dragPos;
        _lerpFrequency = _frequency;
    }

    public void DragVehicle()
    {
        transform.position = Vector2.Lerp(transform.position, _dragPosition.position, _lerpFrequency);
        transform.localScale = Vector2.Lerp(transform.localScale, Vector2.zero, _lerpFrequency);

        if(Vector3.Distance(transform.position,_dragPosition.position) < 0.1f)
        {
            Destroy(gameObject);
        }
    }

}
