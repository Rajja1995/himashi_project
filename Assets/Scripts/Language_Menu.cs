﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class Language_Menu : MonoBehaviour
{
    public GameObject _overFlowMenu;
    public GameObject _selectLanguagePage;
    public GameObject _instructionPage;
    public GameObject _instructionsLevel_1;
    public GameObject _instructionsLevel_2;

    public TextMeshProUGUI _levelNumber;
    public TextMeshProUGUI _stepNumber;

    [Header("Weekly Progress")]
    public GameObject _weeklyProgressPage;
    public GameObject _table;
    public GameObject _notification;
    public List<Image> _progressBars;
    public TextMeshProUGUI _title;
    public bool _display = false;

    private int _level;
    private int _step;

    private void Start()
    {
        if (PlayerPrefs.HasKey("Language_Level"))
        {
            _level = PlayerPrefs.GetInt("Language_Level");
            _step = PlayerPrefs.GetInt("Language_Step");
            print("111");
        }
        else
        {
            PlayerPrefs.SetString("Language","Sinhala"); // Set language as sinhala.
            _level = 1;
            _step = 1;

            PlayerPrefs.SetInt("Language_Level", _level);
            PlayerPrefs.SetInt("Language_Step", _step);
        }

        _levelNumber.text = _level.ToString();
        _stepNumber.text = _step.ToString();
    }

    #region Button Clicks

    public void OnClickOverFlowMenu()
    {
        _overFlowMenu.SetActive(true);
    }

    public void OnClickBack()
    {
        _selectLanguagePage.SetActive(false);
        _instructionPage.SetActive(false);
        _weeklyProgressPage.SetActive(false);
    }

    public void OnClickBackToMainMenu()
    {
        SceneManager.LoadScene(2);
    }

    public void OnClickBackGround()
    {
        if (_overFlowMenu.activeInHierarchy)
        {
            _overFlowMenu.SetActive(false);
        }
    }

    public void OnClickLanguage()
    {
        _overFlowMenu.SetActive(false);
        _selectLanguagePage.SetActive(true);
        _instructionPage.SetActive(false);
    }

    public void OnClickInstruction()
    {
        _overFlowMenu.SetActive(false);
        _selectLanguagePage.SetActive(false);
        _instructionPage.SetActive(true);
    }

    public void OnClick_SwitchInstructions()
    {
        if (_instructionsLevel_1.activeInHierarchy)
        {
            _instructionsLevel_1.SetActive(false);
            _instructionsLevel_2.SetActive(true);
        }
        else
        {
            _instructionsLevel_1.SetActive(true);
            _instructionsLevel_2.SetActive(false);
        }
    }

    public void OnClickSelect_Sinhala()
    {
        PlayerPrefs.SetString("Language","Sinhala");
    }

    public void OnClickSelect_English()
    {
        PlayerPrefs.SetString("Language", "English");
    }

    public void OnClickPlayGame()
    {
        if(PlayerPrefs.GetString("Language") == "Sinhala")
        {
            if(_level == 1)
            {
                SceneManager.LoadScene("PicturePuzzle");
            }
            else if(_level == 2)
            {
                SceneManager.LoadScene("JumbleWords");
            }
            else if(_level == 3)
            {
                SceneManager.LoadScene("");
            }
        }
        else if (PlayerPrefs.GetString("Language") == "English")
        {
            if (_level == 1)
            {
                SceneManager.LoadScene("EnPicturePuzzle");
            }
            else if (_level == 2)
            {
                SceneManager.LoadScene("EnPicturePuzzle");
            }
            else if (_level == 3)
            {
                SceneManager.LoadScene("");
            }
        }
    }

    public void OnClickWeeklyProgress()
    {
        _overFlowMenu.SetActive(false);
        _selectLanguagePage.SetActive(false);
        _instructionPage.SetActive(false);
        _weeklyProgressPage.SetActive(true);

        _title.text = "Week " + _level.ToString() + " Progress";

        if (_step == 1)
        {
            _notification.SetActive(true);
            _table.SetActive(false);
        }
        else
        {
            _notification.SetActive(false);
            _table.SetActive(true);

            if (!_display)
            {
                _display = true;

                if (_level == 1)
                {
                    for (int i = 0; i < PlayerPrefs.GetInt("Week1Day"); i++)
                    {
                        float fillamount = (float)PlayerPrefs.GetInt("Language_Score_Week1" + i) / 150;
                        _progressBars[i].fillAmount = fillamount;
                        print(PlayerPrefs.GetInt("Language_Score_Week1" + i)+","+ fillamount);
                    }
                }
                else if(_level == 2)
                {
                    for (int i = 0; i < PlayerPrefs.GetInt("Week2Day"); i++)
                    {
                        float fillamount = (float)PlayerPrefs.GetInt("Language_Score_Week2" + i) / 150;
                        _progressBars[i].fillAmount = fillamount;
                    }
                }
                else if(_level == 3)
                {
                    for (int i = 0; i < PlayerPrefs.GetInt("Week3Day"); i++)
                    {
                        float fillamount = (float)PlayerPrefs.GetInt("Language_Score_Week3" + i) / 150;
                        _progressBars[i].fillAmount = fillamount;
                    }
                }
            }
        }
    }

    #endregion
}
