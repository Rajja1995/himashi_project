﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class EF_CountdownTimer : MonoBehaviour
{
    private GameManagerScript GMS;
    
    [SerializeField] private float maxTime = 180.0f;
    public Text timerSeconds;

    // Start is called before the first frame update
    void Start()
    {
        GMS = GameObject.Find("Manager").GetComponent<GameManagerScript>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GMS.counterDownDone == true)
        {
            maxTime -= Time.deltaTime;
            timerSeconds.text = maxTime.ToString("f2");
            if (maxTime <= 0.0f)
            {
                GMS.counterDownDone = false;
                GMS.SetCutSeen();
            }
            if (maxTime <= 5.0f)
            {
                print(maxTime);
                timerSeconds.color = Color.red;
            }
        }
    }
}
