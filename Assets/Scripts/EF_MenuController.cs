﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
public class EF_MenuController : MonoBehaviour
{
    public GameObject levelDisplay;
    public GameObject instructionPage;
    public TextMeshProUGUI levelNumberText;
    public TextMeshProUGUI stepNumberText;
    private int levelNumber;
    private int stepNumber;

    [Header("OverFlowMenu")]
    public GameObject _overFlowMenu;

    [Header("OverFlowMenu")]
    public GameObject _weeklyProgress;
    public GameObject _notification;
    public GameObject _table;
    public TextMeshProUGUI _week;
    public List<Image> _progressBars;
    public bool _display = false;

    [Header("Instruction")]
    public List<GameObject> _instruction;
    private int index = 0;
    public GameObject _nextBtn;
    public GameObject _menuBtn;

    private void Start()
    {
        //PlayerPrefs.DeleteAll();
        if (PlayerPrefs.HasKey("EF_Level"))
        {
            levelNumber = PlayerPrefs.GetInt("EF_Level");
            stepNumber = PlayerPrefs.GetInt("EF_Step");
        }
        else
        {
            levelNumber = 1;
            stepNumber = 1;

            PlayerPrefs.SetInt("EF_Level", levelNumber);
            PlayerPrefs.SetInt("EF_Step", stepNumber);
        }
        
    }

    public void OnClickPlayGame()
    {
        levelDisplay.SetActive(true);

        levelNumberText.text = levelNumber.ToString();
        stepNumberText.text = stepNumber.ToString();
    }

    public void OnClickStartGame()
    {
        if(levelNumber == 1)
        {
            SceneManager.LoadScene("EF_Week1");
        }
        else if (levelNumber == 2)
        {
            SceneManager.LoadScene("EF_Week2");
        }
        else if (levelNumber == 3)
        {
            SceneManager.LoadScene("EF_Week3");
        }
        else if (levelNumber == 4)
        {
            SceneManager.LoadScene("EF_Week4");
        }
    }

    public void OnClickMenu()
    {
        levelDisplay.SetActive(false);
        instructionPage.SetActive(false);
        _weeklyProgress.SetActive(false);

        _nextBtn.SetActive(true);
        _menuBtn.SetActive(false);
        index = 0;
    }

    public void OnClickOverFlowMenu()
    {
        _overFlowMenu.SetActive(true);
    }

    public void OnClickBackGround()
    {
        if (_overFlowMenu.activeInHierarchy)
        {
            _overFlowMenu.SetActive(false);
        }
    }

    public void OnClickWeeklyProgress()
    {
        _weeklyProgress.SetActive(true);
        _overFlowMenu.SetActive(false);

        if (stepNumber == 1)
        {
            _notification.SetActive(true);
            _table.SetActive(false);
        }
        else
        {
            if (!_display)
            {
                _display = true;
                _notification.SetActive(false);
                _table.SetActive(true);

                if (levelNumber == 1)
                {
                    _week.text = "Week 1 Progress";

                    for (int i = 0; i < PlayerPrefs.GetInt("Week1Day"); i++)
                    {
                        float fillAmount = (float)PlayerPrefs.GetInt("EF_ScoreWeek1" + i) / 100.0f;
                        _progressBars[i].fillAmount = fillAmount;
                    }
                }
                if (levelNumber == 2)
                {
                    _week.text = "Week 2 Progress";

                    for (int i = 0; i < PlayerPrefs.GetInt("Week2Day"); i++)
                    {
                        float fillAmount = (float)PlayerPrefs.GetInt("EF_ScoreWeek2" + i) / 100.0f;
                        _progressBars[i].fillAmount = fillAmount;
                    }
                }
                if (levelNumber == 3)
                {
                    _week.text = "Week 3 Progress";

                    for (int i = 0; i < PlayerPrefs.GetInt("Week3Day"); i++)
                    {
                        float fillAmount = (float)PlayerPrefs.GetInt("EF_ScoreWeek3" + i) / 100.0f;
                        _progressBars[i].fillAmount = fillAmount;
                    }
                }
                if (levelNumber == 4)
                {
                    _week.text = "Week 4 Progress";

                    for (int i = 0; i < PlayerPrefs.GetInt("Week4Day"); i++)
                    {
                        float fillAmount = (float)PlayerPrefs.GetInt("EF_ScoreWeek4" + i) / 100.0f;
                        _progressBars[i].fillAmount = fillAmount;
                    }
                }
            }
        }
    }

    public void OnClickBacktoMainMenu()
    {
        SceneManager.LoadScene(2);
    }

    public void OnClickNextBtn()
    {
        for(int i = 0; i < _instruction.Count; i++)
        {
            if(i == index)
            {
                _instruction[i].SetActive(true);
            }
            else
            {
                _instruction[i].SetActive(false);
            }
        }

        if(index == 3)
        {
            _nextBtn.SetActive(false);
            _menuBtn.SetActive(true);
        }
        else
        {
            index++;
        }
    }

    public void OnClickInstruction()
    {
        instructionPage.SetActive(true);
    }
}
