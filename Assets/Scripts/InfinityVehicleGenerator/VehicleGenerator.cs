﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum VehicleType
{
    LAND_4_WHEEL,
    LAND_2_WHEEL,
    WATER,
    AIR
}

public class VehicleGenerator : MonoBehaviour
{
    public GameObject _vehicleHandler; // Prefab
    public float _speed = 5.0f;
    public float _minSpeed = 5.0f;
    [SerializeField] private float _startSpeed = 1.0f;
    public VehicleType _vehicleType;
    public GameObject _currentSpawnVehicle;

    public Transform _vehicleSpawnPosition;

    public List<GameObject> _activeVehicles;

    [Header("Land Vehicles")]
    public List<Sprite> _landVehicles_4_Wheel;
    public List<Sprite> _landVehicles_2_Wheel;

    [Header("Water Vehicles")]
    public List<Sprite> _waterVehicles;

    [Header("Air Vehicles")]
    public List<Sprite> _airVehicles;

    public static VehicleGenerator Instance;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        if (GameData.Instance._isFirstTimeStart)
        {
            _speed = _startSpeed;
            _minSpeed = _startSpeed;
        }
        else
        {
            _speed = PlayerPrefs.GetFloat("Vehicle_Speed");
        }

        SelectVehicleType();
    }

    public void SelectVehicleType()
    {
        int _type = Random.Range(0, MissionController.Instance._activateVehicleTypes);

        if(_type == 1)
        {
            SpawnVehicle(VehicleType.LAND_2_WHEEL);
        }

        if (_type == 0)
        {
            SpawnVehicle(VehicleType.LAND_4_WHEEL);
        }

        if (_type == 2)
        {
            SpawnVehicle(VehicleType.WATER);
        }

        if (_type == 3)
        {
            SpawnVehicle(VehicleType.AIR);
        }

    }

    public void SpawnVehicle(VehicleType _vehicleType)
    {
        _currentSpawnVehicle = Instantiate(_vehicleHandler);
        _currentSpawnVehicle.transform.position = _vehicleSpawnPosition.position;
        _currentSpawnVehicle.transform.parent = transform;

        if (_vehicleType == VehicleType.LAND_2_WHEEL)
        {
            int _type = Random.Range(0, _landVehicles_2_Wheel.Count);
            _currentSpawnVehicle.GetComponent<SpriteRenderer>().sprite = _landVehicles_2_Wheel[_type];
            _currentSpawnVehicle.tag = "Land_2_Wheel";
        }

        if (_vehicleType == VehicleType.LAND_4_WHEEL)
        {
            int _type = Random.Range(0, _landVehicles_4_Wheel.Count);
            _currentSpawnVehicle.GetComponent<SpriteRenderer>().sprite = _landVehicles_4_Wheel[_type];
            _currentSpawnVehicle.tag = "Land_4_Wheel";
        }

        if (_vehicleType == VehicleType.WATER)
        {
            int _type = Random.Range(0, _waterVehicles.Count);
            _currentSpawnVehicle.GetComponent<SpriteRenderer>().sprite = _waterVehicles[_type];
            _currentSpawnVehicle.tag = "Water";

        }

        if (_vehicleType == VehicleType.AIR)
        {
            int _type = Random.Range(0, _airVehicles.Count);
            _currentSpawnVehicle.GetComponent<SpriteRenderer>().sprite = _airVehicles[_type];
            _currentSpawnVehicle.tag = "Air";
        }
        
        _activeVehicles.Add(_currentSpawnVehicle);
    }

    public void DeleteVehicle()
    {
        Destroy(_activeVehicles[0]);
        _activeVehicles.RemoveAt(0);
    }
}
