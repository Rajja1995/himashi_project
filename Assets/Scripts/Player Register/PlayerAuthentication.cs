﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using PlayFab;
using PlayFab.ClientModels;
using TMPro;
using UnityEngine.UI;

public class PlayerAuthentication : MonoBehaviour
{
    [Header("Input Fields")]
    public string _userName;
    public string _password;
    public string _patientName;
    public string _patientAge;
    public string _patientResult;
    public string _careGiverName;
    public string _careGiverMail;
    public string _doctorName;
    public string _doctorMail;

    [Header("Sign In Input Fields")]
    public TMP_InputField _userNameSignIn;
    public TMP_InputField _passwordSignIn;

    [Header("Sign Up Input Fields")]
    public List<TMP_InputField> _playerDataInputs;
    public TMP_InputField _userNameSignUp;
    public TMP_InputField _passwordSignUp;

    public GameObject _loadingImage;
    public Animator _anim;

    public TextMeshProUGUI _notificationBar;
    public string _playFabID;
    public string _attention;
    public string _concentration;

    public static PlayerAuthentication Instance;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        //PlayerPrefs.DeleteAll();
        _notificationBar.text = "";

        //Note: Setting title Id here can be skipped if you have set the value in Editor Extensions already.
        if (string.IsNullOrEmpty(PlayFabSettings.TitleId))
        {
            PlayFabSettings.TitleId = "789AA"; // Please change this value to your own titleId from PlayFab Game Manager
        }
        //var request = new LoginWithCustomIDRequest { CustomId = "GettingStartedGuide", CreateAccount = true };
        //PlayFabClientAPI.LoginWithCustomID(request, OnLoginSuccess, OnLoginFailure);

        //if (PlayerPrefs.HasKey("UserName"))
        //{
        //    _userName = PlayerPrefs.GetString("UserName");
        //    _password = PlayerPrefs.GetString("Password");

        //    var req = new LoginWithPlayFabRequest { Username = _userName, Password = _password };
        //    PlayFabClientAPI.LoginWithPlayFab(req, OnLoginSuccess, OnLoginFailure);
        //}
    }

    public void OnClickCreateAccount()
    {
        SoundManager.Instance.PlayButtonClickSound();
        _anim.SetBool("Show", true);
        _notificationBar.text = "";
    }

    public void OnClickBackBtn()
    {
        SoundManager.Instance.PlayButtonClickSound();
        // Back Btn in signUp page
        _anim.SetBool("Show", false);
        _notificationBar.text = "";
    }

    #region Player Registration

    public void OnClickSignUp()
    {
        SoundManager.Instance.PlayButtonClickSound();
        _notificationBar.text = "";
        _loadingImage.SetActive(true);
        bool empty = false;
        for(int i = 0; i < _playerDataInputs.Count ; i++)
        {
            if(_playerDataInputs[i].text == "")
            {
                empty = true;
            }
        }

        if(!empty)
        {
            if(_userName.Length <= 3)
            {
                _loadingImage.SetActive(false);
                _notificationBar.text = "Username should have atleast 4 characters";
            }
            else
            {
                var req = new RegisterPlayFabUserRequest { Username = _userName, Password = _password , RequireBothUsernameAndEmail = false};
                PlayFabClientAPI.RegisterPlayFabUser(req, OnRegisterSuccess, OnRegisterFailure);
            }
        }
        else
        {
            _loadingImage.SetActive(false);
            _notificationBar.text = "Please fill the required fields.";
        }
    }

    public void OnClickSignIn()
    {
        SoundManager.Instance.PlayButtonClickSound();
        _notificationBar.text = "";
        _loadingImage.SetActive(true);

        if (_userName != "" && _password != "")
        {
            var req = new LoginWithPlayFabRequest { Username = _userName, Password = _password};
            PlayFabClientAPI.LoginWithPlayFab(req, OnLoginSuccess, OnLoginFailure);
            PlayerPrefs.SetString("UserName", _userName);
            PlayerPrefs.SetString("Password", _password);
        }
        else
        {
            _loadingImage.SetActive(false);
            _notificationBar.text = "Please fill the required fields.";
        }
    }

    private void OnLoginSuccess(LoginResult result)
    {
        print("login success");
        SceneManager.LoadScene(2);
        _playFabID = result.PlayFabId;
        GetUserData(_playFabID);
    }

    private void OnLoginFailure(PlayFabError error)
    {
        if (PlayerPrefs.HasKey("UserName"))
        {
            // invalid
            _loadingImage.SetActive(false);
            _notificationBar.text = "Invalid username or password.";
        }
        else
        {
            // create account
            _loadingImage.SetActive(false);
            _notificationBar.text = "Please sign up before sign in.";
        }

        ClearSignInPage();
    }

    private void OnRegisterSuccess(RegisterPlayFabUserResult result)
    {
        Debug.Log("Registered!");

        // Update palayer data.
        PlayerPrefs.SetString("UserName", _userName);
        PlayerPrefs.SetString("Password", _password);
        PlayerPrefs.SetString("PatientName", _patientName);
        PlayerPrefs.SetString("PatientAge", _patientAge);
        PlayerPrefs.SetString("PatientResult", _patientResult);
        PlayerPrefs.SetString("CareGiverName", _careGiverName);
        PlayerPrefs.SetString("CareGiverMail", _careGiverMail);
        PlayerPrefs.SetString("DoctorName", _doctorName);
        PlayerPrefs.SetString("DoctorMail", _doctorMail);

        SetUserData();

        SceneManager.LoadScene(2);
    }

    private void OnRegisterFailure(PlayFabError error)
    {

        // already exists
        //Debug.LogError(error.GenerateErrorReport());
        _loadingImage.SetActive(false);
        _notificationBar.text = "Username or password you entered is already exsist.";

        ClearSignUpPage();
    }

    #endregion

    #region Get Input Fields

    public void GetUserNameSignUp(string userName)
    {
        _userNameSignUp.text = _userNameSignUp.text.Replace(" ", "");
        _userName = _userNameSignUp.text;
        print(_userName);
    }

    public void GetPasswordSignUp(string password)
    {
        _passwordSignUp.text = _passwordSignUp.text.Replace(" ", "");
        _password = _passwordSignUp.text;
    }

    public void GetUserNameSignIn(string userName)
    {
        _userNameSignIn.text = _userNameSignIn.text.Replace(" ", "");
        _userName = _userNameSignIn.text;
    }

    public void GetPasswordSignIn(string password)
    {
        _passwordSignIn.text = _passwordSignIn.text.Replace(" ", "");
        _password = _passwordSignIn.text;
    }

    public void GetPatientName(string patientName)
    {
        _patientName = patientName;
    }

    public void GetPatientAge(string patientAge)
    {
        _patientAge = patientAge;
    }

    public void GetPatientResult(string result)
    {
        _patientResult = result;
    }

    public void GetCareGiverName(string name)
    {
        _careGiverName = name;
    }

    public void GetCareGiverMail(string mail)
    {
        _careGiverMail = mail;
    }

    public void GetDoctorName(string name)
    {
        _doctorName = name;
    }

    public void GetDoctorMail(string mail)
    {
        _doctorMail = mail;
    }

    #endregion

    #region Clear Input Data

    public void ClearSignUpPage()
    {
        _userNameSignUp.text = "";
        _passwordSignUp.text = "";
    }

    public void ClearSignInPage()
    {
        _userNameSignIn.text = "";
        _passwordSignIn.text = "";
    }

    #endregion

    public void SetUserData()
    {
        PlayFabClientAPI.UpdateUserData(new UpdateUserDataRequest()
        {
            Data = new Dictionary<string, string>() {
            {"Patient Name", _patientName},
            {"Patient Age", _patientAge},
            {"Patient Result", _patientResult},
            {"CareGiver Name", _careGiverName},
            {"CareGiver Mail", _careGiverMail},
            {"Doctor Name", _doctorName},
            {"Doctor Mail", _doctorMail},
            {"Attention", ""},
            {"Concentration", ""}
        }
        },
    result => Debug.Log("Successfully updated user data"),
    error => {
        Debug.Log("Failed");
        Debug.Log(error.GenerateErrorReport());
    });
    }

    public void GetUserData(string myPlayFabeId)
    {
        PlayFabClientAPI.GetUserData(new GetUserDataRequest()
        {
            PlayFabId = myPlayFabeId,
            Keys = null
        }, result => {
            Debug.Log("Got user data:");
            if (result.Data == null) {
                Debug.Log("No data");
            } 
            else 
            {
                //Debug.Log("Ancestor: " + result.Data["Ancestor"].Value);
                // Update palayer data.
                PlayerPrefs.SetString("PatientName", result.Data["Patient Name"].Value);
                PlayerPrefs.SetString("PatientAge", result.Data["Patient Age"].Value);
                PlayerPrefs.SetString("PatientResult", result.Data["Patient Result"].Value);
                PlayerPrefs.SetString("CareGiverName", result.Data["CareGiver Name"].Value);
                PlayerPrefs.SetString("CareGiverMail", result.Data["CareGiver Mail"].Value);
                PlayerPrefs.SetString("DoctorName", result.Data["Doctor Name"].Value);
                PlayerPrefs.SetString("DoctorMail", result.Data["Doctor Mail"].Value);
                _attention = result.Data["Attention"].Value;
                _concentration = result.Data["Concentration"].Value;
            }
        }, (error) => {
            Debug.Log("Got error retrieving user data:");
            Debug.Log(error.GenerateErrorReport());
        });
    }

    public void UpdateUserProgressData(int atention,int concentration)
    {
        _attention = _attention  + atention.ToString() + ", ";
        _concentration = _concentration  + concentration.ToString() + ", ";

        PlayFabClientAPI.UpdateUserData(new UpdateUserDataRequest()
        {
            Data = new Dictionary<string, string>() {
            {"Attention", _attention},
            {"Concentration", _concentration}
        }
        },
    result => Debug.Log("Successfully updated user data"),
    error => {
        Debug.Log("Failed");
        Debug.Log(error.GenerateErrorReport());
    });
    }
}