﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameData : MonoBehaviour
{
    public static GameData Instance;

    public float _speed;
    public int _vehicleCount;
    public int _correctTries;
    public int _incorrectTries;
    public int _missedTries;

    public int _levelNumber = 1;
    public int _stepNumber = 1;

    public bool _isFirstTimeStart = true;

    [Header("ML")]
    public float performance;
    public int avgtime;
    public int _predict_count = 1;
    public int totaltime;
    public float _totaltime = 0f;
    public float _avg_time;

    [Header("Last Play Time")]
    private DateTime _lastPlayTime;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        if (PlayerPrefs.HasKey("IsFirstTime"))
        {
            if (PlayerPrefs.GetString("IsFirstTime") == "False")
            {
                _isFirstTimeStart = false;
            }
            else
            {
                _isFirstTimeStart = true;
            }
        }
        else
        {
            _isFirstTimeStart = true;
        }

        if (!_isFirstTimeStart)
        {
            GetPlayerData();
        }
    }

    public void UpdatePlayerStat(int correct, int incorrect,int missed)
    {
        // Set wrong,correct and missed count
        _correctTries += correct;
        _incorrectTries += incorrect;
        _missedTries += missed;

        PlayerPrefs.SetInt("Correct_Tries", _correctTries);
        PlayerPrefs.SetInt("Incorrect_Tries", _incorrectTries);
        PlayerPrefs.SetInt("Missed_Tries", _missedTries);
    }

    public void UpdateLevel()
    {
        // Set level
        _levelNumber += 1;
        PlayerPrefs.SetInt("Level_Number", _levelNumber);
    }

    public void UpdateStep(int steps)
    {
        _stepNumber = steps;
        PlayerPrefs.SetInt("Step_Number", _stepNumber);

        PlayerPrefs.SetString("IsFirstTime", "False");
    }

    public void updateGameData(float speed, int vehicleCount, DateTime _today)
    {
        // Set new speed by ML
        _speed = speed;
        PlayerPrefs.SetFloat("Vehicle_Speed", _speed);

        // Set new vehicle count for next step
        _vehicleCount = vehicleCount;
        PlayerPrefs.SetInt("Vehicle_Count", _vehicleCount);

        // Set Date
        _lastPlayTime = _today;
        PlayerPrefs.SetString("Last_Play_Date", _lastPlayTime.ToString());
    }

    public void GetPlayerData()
    {
        _correctTries = PlayerPrefs.GetInt("Correct_Tries");
        _incorrectTries = PlayerPrefs.GetInt("Incorrect_Tries");
        _missedTries = PlayerPrefs.GetInt("Missed_Tries");
        _speed = PlayerPrefs.GetInt("Vehicle_Speed");
        _vehicleCount = PlayerPrefs.GetInt("Vehicle_Count");
        _stepNumber = PlayerPrefs.GetInt("_stepNumber");
        _levelNumber = PlayerPrefs.GetInt("Level_Number");

        _predict_count = PlayerPrefs.GetInt("Predict_Count");
        _totaltime = PlayerPrefs.GetInt("Time_Count");
        _avg_time = PlayerPrefs.GetInt("avg_time");
        //qlearning();
    }

    public float qlearning()
    {
        int ql_try_Penalty = Convert.ToInt32(((_correctTries * (20 / 100))) * _missedTries + _incorrectTries);
        int ql_missed_Penalty = Convert.ToInt32(((_correctTries * (5 / 100))) * _missedTries);
        int ql_panelty = Mathf.RoundToInt(ql_missed_Penalty + ql_try_Penalty);

        float perf = (_correctTries + _incorrectTries + _missedTries) - ql_panelty;

        perf = perf / (_correctTries + _incorrectTries + _missedTries);
        perf = perf * 100.0f;
        performance = Mathf.RoundToInt(perf);
        //Debug.Log("perf : " + perf);
        _predict_count += 1;
        PlayerPrefs.SetInt("Predict_Count", _predict_count);
        // Debug.Log("_predict_count : " + _predict_count);
        PlayerPrefs.SetInt("Time_Count", totaltime);
        //Debug.Log("Time_Count save: " + _totaltime);

        avgtime = ((int)(_totaltime / _predict_count));
        PlayerPrefs.SetInt("avg_time", avgtime);
        //Debug.Log("avg_time: " + avgtime);

        return performance;
    }

    public float chooseDifficulty(float difficulty)
    {
        //Debug.Log(" chooseDifficulty : " + performance);
        float averagePerformance = difficulty;

        Debug.Log(averagePerformance);
        Debug.Log(avgtime);
        if (averagePerformance >= 90.0f)
        {
            Debug.Log("-- 90 -- ");
            if (avgtime > 30)
            {
                _speed = (VehicleGenerator.Instance._speed + 0.200f);
                PlayerPrefs.SetFloat("Vehicle_Speed", _speed);
                //Debug.Log(_speed);
                return _speed;
            }
            else if (avgtime > 25)
            {
                _speed = (VehicleGenerator.Instance._speed + 0.184f);
                PlayerPrefs.SetFloat("Vehicle_Speed", _speed);
                //Debug.Log(_speed);
                return _speed;
            }
            else if (avgtime > 20)
            {
                _speed = (VehicleGenerator.Instance._speed + 0.180f);
                PlayerPrefs.SetFloat("Vehicle_Speed", _speed);
                //Debug.Log(_speed);
                return _speed;
            }
            else if (avgtime > 15)
            {
                _speed = (VehicleGenerator.Instance._speed + 0.176f);
                PlayerPrefs.SetFloat("Vehicle_Speed", _speed);
                //Debug.Log(_speed);
                return _speed;
            }
            else if (avgtime > 10)
            {
                _speed = (VehicleGenerator.Instance._speed + 0.172f);
                PlayerPrefs.SetFloat("Vehicle_Speed", _speed);
                //Debug.Log(_speed);
                return _speed;
            }
            else if (avgtime > 5)
            {
                _speed = (VehicleGenerator.Instance._speed + 0.168f);
                PlayerPrefs.SetFloat("Vehicle_Speed", _speed);
                //Debug.Log(_speed);
                return _speed;
            }
            else
            {
                _speed = (VehicleGenerator.Instance._speed + 0.164f);
                PlayerPrefs.SetFloat("Vehicle_Speed", _speed);
                //Debug.Log(_speed);
                return _speed;
            }
        }

        else if (averagePerformance >= 75.0f)
        {
            Debug.Log("-- 75 -- ");
            if (avgtime > 30)
            {
                _speed = (VehicleGenerator.Instance._speed + 0.160f);
                PlayerPrefs.SetFloat("Vehicle_Speed", _speed);
                //Debug.Log(_speed);
                return _speed;
            }
            else if (avgtime > 25)
            {
                _speed = (VehicleGenerator.Instance._speed + 0.156f);
                PlayerPrefs.SetFloat("Vehicle_Speed", _speed);
                //Debug.Log(_speed);
                return _speed;
            }
            else if (avgtime > 20)
            {
                _speed = (VehicleGenerator.Instance._speed + 0.152f);
                PlayerPrefs.SetFloat("Vehicle_Speed", _speed);
                //Debug.Log(_speed);
                return _speed;
            }
            else if (avgtime > 15)
            {
                _speed = (VehicleGenerator.Instance._speed + 0.148f);
                PlayerPrefs.SetFloat("Vehicle_Speed", _speed);
                //Debug.Log(_speed);
                return _speed;
            }
            else if (avgtime > 10)
            {
                _speed = (VehicleGenerator.Instance._speed + 0.144f);
                PlayerPrefs.SetFloat("Vehicle_Speed", _speed);
                //Debug.Log(_speed);
                return _speed;
            }
            else if (avgtime > 5)
            {
                _speed = (VehicleGenerator.Instance._speed + 0.140f);
                PlayerPrefs.SetFloat("Vehicle_Speed", _speed);
                //Debug.Log(_speed);
                return _speed;
            }
            else
            {
                _speed = (VehicleGenerator.Instance._speed + 0.136f);
                PlayerPrefs.SetFloat("Vehicle_Speed", _speed);
                //Debug.Log(_speed);
                return _speed;
            }
        }

        else if (averagePerformance >= 65.0f)
        {
            Debug.Log("-- 65 -- ");
            _speed = (VehicleGenerator.Instance._speed + 0.12f);
            PlayerPrefs.SetFloat("Vehicle_Speed", _speed);
            //Debug.Log(_speed);
            return _speed;

        }
        else if (averagePerformance >= 50.0f)
        {
            Debug.Log("-- 50 -- ");
            _speed = (VehicleGenerator.Instance._speed + 0.10f);
            PlayerPrefs.SetFloat("Vehicle_Speed", _speed);
            //Debug.Log(_speed);
            return _speed;

        }
        else if (averagePerformance >= 35.0f)
        {
            Debug.Log("-- 35 -- ");
            _speed = (VehicleGenerator.Instance._speed + 0.08f);
            PlayerPrefs.SetFloat("Vehicle_Speed", _speed);
            //Debug.Log(_speed);
            return _speed;

        }
        else
        {
            //Debug.Log("-- --- -- ");
            _speed = (VehicleGenerator.Instance._speed - 0.2f);
            PlayerPrefs.SetFloat("Vehicle_Speed", _speed);
            //Debug.Log(_speed);
            return _speed;
        }
    }
}
