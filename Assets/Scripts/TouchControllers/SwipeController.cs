﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SwipeController : MonoBehaviour
{
    public bool _tap, _swipeLeft, _swipeRight, _swipeBottomLeftCorner, _swipeBottomRightCorner;
    private bool _isDraging = false;
    private Vector2 _startTouch, _swipeDelta,_endTouch;

    public ScoreManager _scoreManager;

    // Drag Positions
    public Transform _left;
    public Transform _right;
    public Transform _leftBottom;
    public Transform _rightBottom;

    [SerializeField] private float _dragFrequency = 0.7f;

    public GameObject _touchObject;

    private void Update()
    {
        if (!GameManager.Instance._isPauseGame)
        {
            _swipeLeft = _swipeRight = _isDraging = _swipeBottomLeftCorner = _swipeBottomRightCorner = false;

            #region Standalone Inputs
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

                if (hit)
                {
                    _touchObject = hit.transform.gameObject;
                    _tap = true;
                    _startTouch = Input.mousePosition;
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                _tap = false;
                Reset();
            }
            #endregion

            #region Mobile Input
            if (Input.touches.Length > 0)
            {
                if (Input.touches[0].phase == TouchPhase.Began)
                {
                    RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

                    if (hit)
                    {
                        _touchObject = hit.transform.gameObject;
                        _tap = true;
                        _startTouch = Input.touches[0].position;
                    }
                }
                else if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
                {
                    _tap = false;
                    Reset();
                }
            }
            #endregion

            #region Calculate Touch Inputs

            //Calculate the distance
            _swipeDelta = Vector2.zero;

            if (_tap)
            {
                if (Input.touches.Length > 0)
                {
                    _swipeDelta = Input.touches[0].position - _startTouch;
                    _endTouch = Input.touches[0].position;
                }
                else if (Input.GetMouseButton(0))
                {
                    _swipeDelta = (Vector2)Input.mousePosition - _startTouch;
                    _endTouch = Input.mousePosition;
                }

                if (_swipeDelta.magnitude > 10)
                {
                    _tap = false;
                    _isDraging = true;
                    //Which direction?
                    float x = _swipeDelta.x;
                    float y = _swipeDelta.y;

                    if(_endTouch.y < _startTouch.y)
                    {
                        if (_endTouch.x < _startTouch.x)
                        {
                            _swipeBottomLeftCorner = true;
                        }
                        else
                        {
                            _swipeBottomRightCorner = true;
                        }
                    }
                    else if (_endTouch.y >= _startTouch.y)
                    {
                        if (_endTouch.x < _startTouch.x)
                        {
                            _swipeLeft = true;
                        }
                        else
                        {
                            _swipeRight = true;
                        }
                    }

                    Reset();
                }
            }
            #endregion

            if (_swipeRight)
            {
                if (MissionController.Instance._level != Levels.LEVEL_1 && MissionController.Instance._level != Levels.LEVEL_2)
                {
                    VehicleGenerator.Instance._activeVehicles.Remove(_touchObject);
                    _touchObject.GetComponent<VehicleController>().SetDragPosition(_right, _dragFrequency);
                    _scoreManager.RightSwipe(_touchObject.tag);
                    _swipeRight = false;
                    _touchObject = null;
                }
            }

            if (_swipeLeft)
            {
                if (MissionController.Instance._level != Levels.LEVEL_1)
                {
                    VehicleGenerator.Instance._activeVehicles.Remove(_touchObject);
                    _touchObject.GetComponent<VehicleController>().SetDragPosition(_left, _dragFrequency);
                    _scoreManager.LeftSwipe(_touchObject.tag);
                    _swipeLeft = false;
                    _touchObject = null;
                }
            }

            if (_swipeBottomLeftCorner)
            {
                VehicleGenerator.Instance._activeVehicles.Remove(_touchObject);
                _touchObject.GetComponent<VehicleController>().SetDragPosition(_leftBottom, _dragFrequency);
                _scoreManager.LeftBottom(_touchObject.tag);
                _swipeBottomLeftCorner = false;
                _touchObject = null;
            }

            if (_swipeBottomRightCorner)
            {
                VehicleGenerator.Instance._activeVehicles.Remove(_touchObject);
                _touchObject.GetComponent<VehicleController>().SetDragPosition(_rightBottom, _dragFrequency);
                _scoreManager.RightBottom(_touchObject.tag);
                _swipeBottomRightCorner = false;
                _touchObject = null;
            }
        }
    }

    private void Reset()
    {
        _startTouch = _swipeDelta = Vector2.zero;
        _tap = false;
    }
}
