﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System;
using TMPro;

public class MenuController : MonoBehaviour
{
    [Header("UI Items")]
    public GameObject _countDownUI;
    public GameObject _startButtonUI;
    public TextMeshProUGUI _hours;
    public TextMeshProUGUI _minutes;
    public TextMeshProUGUI _seconds;
    public TextMeshProUGUI _levelNumber_UI;
    public TextMeshProUGUI _stepNumber_UI;

    [Header("OverFlow Menu Items")]
    public TextMeshProUGUI _menuTitle;
    public GameObject _progressHistory;
    public GameObject _weeklyProgress;
    public GameObject _instructions;
    public GameObject _help;

    public bool _canPlay = true;

    [Header("Level Unlock Time")]
    public DateTime _today;
    public DateTime _lastPlayDay;
    public DateTime _nextStepStartTime;
    [SerializeField] private float _maxTimeToUnlock = 300.0f; // Seconds

    [Header("OverFlow Menu")]
    public GameObject _overFlowMenu;
    public Animator _menuAnim;

    [Header("Instruction Page")]
    public GameObject _sinhalaHelp;
    public GameObject _englishHelp;

    [Header("Weekly Progress")]
    public bool _isDisplayProgres = false;
    public GameObject _notification;
    public GameObject _progressPage;
    public TextMeshProUGUI _weekNumber;
    public GameObject[] _graphBars;

    [Header("Progress history")]
    public bool _isDisplayHistory = false;
    public GameObject _listningPrefab;
    public Transform _listningContainer;

    private void Start()
    {
        //PlayerPrefs.DeleteAll();
        if (PlayerPrefs.GetString("IsFirstTime") != "False") // player is beginner
        {
            _canPlay = true;
            UnlockNextStep();

            _levelNumber_UI.text = "1";
            _stepNumber_UI.text = "1";
        }
        else
        {
            _canPlay = false;
            checkDate();
            _levelNumber_UI.text = PlayerPrefs.GetInt("Level_Number").ToString();
            _stepNumber_UI.text = PlayerPrefs.GetInt("Step_Number").ToString();

            print(PlayerPrefs.GetInt("Correct_Tries") +" , "+ PlayerPrefs.GetInt("Incorrect_Tries") + " , " + PlayerPrefs.GetInt("Missed_Tries"));
        }
    }

    private void Update()
    {
        if (!_canPlay)
        {
            // Start Timer.
            _today = DateTime.Now;
            TimeSpan _remainingTime = _nextStepStartTime - _today;
            
            // Display count down
            if(_remainingTime.Hours < 10)
            {
                _hours.text = "0" + _remainingTime.Hours.ToString();
            }
            else
            {
                _hours.text = _remainingTime.Hours.ToString();
            }

            if (_remainingTime.Minutes < 10)
            {
                _minutes.text = "0" + _remainingTime.Minutes.ToString();
            }
            else
            {
                _minutes.text = _remainingTime.Minutes.ToString();
            }

            if (_remainingTime.Seconds < 10)
            {
                _seconds.text = "0" + _remainingTime.Seconds.ToString();
            }
            else
            {
                _seconds.text = _remainingTime.Seconds.ToString();
            }

            if (_today.Subtract(_lastPlayDay) > TimeSpan.FromMinutes(_maxTimeToUnlock))
            {
                UnlockNextStep();
            }
        }

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                if (!EventSystem.current.IsPointerOverGameObject(touch.fingerId))
                {
                    if (_overFlowMenu.activeInHierarchy == true)
                    {
                        _overFlowMenu.SetActive(false);
                    }
                }
            }
        }

        //if (Input.GetMouseButtonDown(0))
        //{
        //    if (!EventSystem.current.IsPointerOverGameObject())
        //    {
        //        if (_overFlowMenu.activeInHierarchy == true)
        //        {
        //            _overFlowMenu.SetActive(false);
        //        }
        //    }
        //    else
        //    {
        //        print("4");
        //    }
        //}
    }

    public void checkDate()
    {
        _today = DateTime.Now;
        _lastPlayDay = Convert.ToDateTime(PlayerPrefs.GetString("Last_Play_Date"));

        _nextStepStartTime = _lastPlayDay.AddMinutes(_maxTimeToUnlock);

        if (_today.Subtract(_lastPlayDay) >= TimeSpan.FromMinutes(_maxTimeToUnlock))
        {
            // Play Game
            UnlockNextStep();
        }
        else
        {
            // start Timer
            _canPlay = false; 

            _startButtonUI.SetActive(false);
            _countDownUI.SetActive(true);
        }
    }

    public void OnClickBackButton()
    {
        SoundManager.Instance.PlayButtonClickSound();
        _menuAnim.SetBool("Display", false);
    }

    public void OnClickMenuBtn()
    {
        SoundManager.Instance.PlayButtonClickSound();
        SceneManager.LoadScene(2);
    }

    public void OnClickOverFlowBtn()
    {
        SoundManager.Instance.PlayButtonClickSound();
        _overFlowMenu.SetActive(true);
    }

    public void OnClickPlayBtn()
    {
        SoundManager.Instance.PlayButtonClickSound();
        _overFlowMenu.SetActive(false);
        SceneManager.LoadScene(4); // Load Game Scene.
    }

    public void UnlockNextStep()
    {
        _canPlay = true;

        _startButtonUI.SetActive(true);
        _countDownUI.SetActive(false);
    }

    public void OnClickSwitchSinhala()
    {
        SoundManager.Instance.PlayButtonClickSound();
        _sinhalaHelp.SetActive(true);
        _englishHelp.SetActive(false);
    }

    public void OnClickSwitchEnglish()
    {
        SoundManager.Instance.PlayButtonClickSound();
        _sinhalaHelp.SetActive(false);
        _englishHelp.SetActive(true);
    }

    public void OnClickWeeklyProgress()
    {
        SoundManager.Instance.PlayButtonClickSound();
        _menuTitle.text = "Weekly Progress";
        _menuAnim.SetBool("Show", true);
        _weeklyProgress.SetActive(true);
        _instructions.SetActive(false);
        _help.SetActive(false);
        _progressHistory.SetActive(false);
        _overFlowMenu.SetActive(false);
        _menuAnim.SetBool("Display", true);

        if (!_isDisplayProgres)
        {
            _isDisplayProgres = true;

            if (PlayerPrefs.GetInt("Level_Number") == 2)
            {
                _weekNumber.text = "Week 2";
                if (PlayerPrefs.GetInt("Week2Day") == 0)
                {
                    _notification.SetActive(true);
                    _progressPage.SetActive(false);
                }
                else
                {
                    _notification.SetActive(false);
                    _progressPage.SetActive(true);

                    for (int i = 0; i < PlayerPrefs.GetInt("Week2Day"); i++)
                    {
                        _graphBars[i].SetActive(true);

                        _graphBars[i].transform.GetChild(0).gameObject.transform.GetComponent<Image>().fillAmount = PlayerPrefs.GetFloat("AttentionWeek2" + i);
                        _graphBars[i].transform.GetChild(1).gameObject.transform.GetComponent<Image>().fillAmount = PlayerPrefs.GetFloat("ConcentrationWeek2" + i);
                    }
                }
            }
            else if (PlayerPrefs.GetInt("Level_Number") == 3)
            {
                _weekNumber.text = "Week 3";
                if (PlayerPrefs.GetInt("Week3Day") == 0)
                {
                    _notification.SetActive(true);
                    _progressPage.SetActive(false);
                }
                else
                {
                    _notification.SetActive(false);
                    _progressPage.SetActive(true);

                    for (int i = 0; i < PlayerPrefs.GetInt("Week3Day"); i++)
                    {
                        _graphBars[i].SetActive(true);

                        _graphBars[i].transform.GetChild(0).gameObject.transform.GetComponent<Image>().fillAmount = PlayerPrefs.GetFloat("AttentionWeek3" + i);
                        _graphBars[i].transform.GetChild(1).gameObject.transform.GetComponent<Image>().fillAmount = PlayerPrefs.GetFloat("ConcentrationWeek3" + i);
                    }
                }
            }
            else if (PlayerPrefs.GetInt("Level_Number") == 4)
            {
                _weekNumber.text = "Week 4";
                if (PlayerPrefs.GetInt("Week4Day") == 0)
                {
                    _notification.SetActive(true);
                    _progressPage.SetActive(false);
                }
                else
                {
                    _notification.SetActive(false);
                    _progressPage.SetActive(true);

                    for (int i = 0; i < PlayerPrefs.GetInt("Week4Day"); i++)
                    {
                        _graphBars[i].SetActive(true);

                        _graphBars[i].transform.GetChild(0).gameObject.transform.GetComponent<Image>().fillAmount = PlayerPrefs.GetFloat("AttentionWeek4" + i);
                        _graphBars[i].transform.GetChild(1).gameObject.transform.GetComponent<Image>().fillAmount = PlayerPrefs.GetFloat("ConcentrationWeek4" + i);
                    }
                }
            }
            else
            {
                _weekNumber.text = "Week 1";
                if (!PlayerPrefs.HasKey("Week1Day"))
                {
                    _notification.SetActive(true);
                    _progressPage.SetActive(false);
                }
                else
                {
                    _notification.SetActive(false);
                    _progressPage.SetActive(true);

                    for (int i = 0; i < PlayerPrefs.GetInt("Week1Day"); i++)
                    {
                        _graphBars[i].SetActive(true);

                        _graphBars[i].transform.GetChild(0).gameObject.transform.GetComponent<Image>().fillAmount = PlayerPrefs.GetFloat("AttentionWeek1" + i);
                        _graphBars[i].transform.GetChild(1).gameObject.transform.GetComponent<Image>().fillAmount = PlayerPrefs.GetFloat("ConcentrationWeek1" + i);
                    }
                }
            }
        }
    }

    public void OnClickProgressHistory()
    {
        SoundManager.Instance.PlayButtonClickSound();
        _menuTitle.text = "History";
        _menuAnim.SetBool("Show", true);
        _progressHistory.SetActive(true);
        _weeklyProgress.SetActive(false);
        _instructions.SetActive(false);
        _help.SetActive(false);
        _overFlowMenu.SetActive(false);
        _menuAnim.SetBool("Display", true);

        if (!_isDisplayHistory)
        {
            _isDisplayHistory = true;

            for (int i = 0; i < PlayerPrefs.GetInt("Week1Day"); i++)
            {
                GameObject go = Instantiate(_listningPrefab, _listningContainer);
                go.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = "1";
                go.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = (i + 1).ToString();
                go.transform.GetChild(2).gameObject.GetComponent<TextMeshProUGUI>().text = ((int)(PlayerPrefs.GetFloat("AttentionWeek1" + i) * 100)).ToString() + "%";
                go.transform.GetChild(3).gameObject.GetComponent<TextMeshProUGUI>().text = ((int)(PlayerPrefs.GetFloat("ConcentrationWeek1" + i) * 100)).ToString() + "%";
            }

            for (int i = 0; i < PlayerPrefs.GetInt("Week2Day"); i++)
            {
                GameObject go = Instantiate(_listningPrefab, _listningContainer);
                go.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = "2";
                go.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = (i + 1).ToString();
                go.transform.GetChild(2).gameObject.GetComponent<TextMeshProUGUI>().text = ((int)(PlayerPrefs.GetFloat("AttentionWeek2" + i) * 100)).ToString() + "%";
                go.transform.GetChild(3).gameObject.GetComponent<TextMeshProUGUI>().text = ((int)(PlayerPrefs.GetFloat("ConcentrationWeek2" + i) * 100)).ToString() + "%";
            }

            for (int i = 0; i < PlayerPrefs.GetInt("Week3Day"); i++)
            {
                GameObject go = Instantiate(_listningPrefab, _listningContainer);
                go.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = "3";
                go.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = (i + 1).ToString();
                go.transform.GetChild(2).gameObject.GetComponent<TextMeshProUGUI>().text = ((int)(PlayerPrefs.GetFloat("AttentionWeek3" + i) * 100)).ToString() + "%";
                go.transform.GetChild(3).gameObject.GetComponent<TextMeshProUGUI>().text = ((int)(PlayerPrefs.GetFloat("ConcentrationWeek3" + i) * 100)).ToString() + "%";
            }

            for (int i = 0; i < PlayerPrefs.GetInt("Week4Day"); i++)
            {
                GameObject go = Instantiate(_listningPrefab, _listningContainer);
                go.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = "4";
                go.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = (i + 1).ToString();
                go.transform.GetChild(2).gameObject.GetComponent<TextMeshProUGUI>().text = ((int)(PlayerPrefs.GetFloat("AttentionWeek4" + i) * 100)).ToString() + "%";
                go.transform.GetChild(3).gameObject.GetComponent<TextMeshProUGUI>().text = ((int)(PlayerPrefs.GetFloat("ConcentrationWeek4" + i) * 100)).ToString() + "%";
            }
        }
    }

    public void OnClickInstructionsBtn()
    {
        SoundManager.Instance.PlayButtonClickSound();
        _menuTitle.text = "Instrutions";
        _instructions.SetActive(true);
        _weeklyProgress.SetActive(false);
        _help.SetActive(false);
        _progressHistory.SetActive(false);
        _overFlowMenu.SetActive(false);
        _menuAnim.SetBool("Display", true);
    }

    public void OnClickHelpBtn()
    {
        SoundManager.Instance.PlayButtonClickSound();
        _menuTitle.text = "Help";
        _help.SetActive(true);
        _weeklyProgress.SetActive(false);
        _instructions.SetActive(false);
        _progressHistory.SetActive(false);
        _overFlowMenu.SetActive(false);
        _menuAnim.SetBool("Display", true);
    }
}
