﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.EventSystems;

public class MainMenuController : MonoBehaviour
{
    public GameObject _overFlowMenu;
    public Animator _menuAnim;

    public GameObject _profile;
    public TextMeshProUGUI _menuTitle;

    [Header("Profile Details")]
    public TextMeshProUGUI _userName;
    public TextMeshProUGUI _password;
    public TextMeshProUGUI _patientName;
    public TextMeshProUGUI _patientAge;
    public TextMeshProUGUI _patientResult;
    public TextMeshProUGUI _patientMail;
    public TextMeshProUGUI _careGiverName;
    public TextMeshProUGUI _careGiverMail;
    public TextMeshProUGUI _doctorName;
    public TextMeshProUGUI _doctorMail;

    [Header("Game 1")]
    public List<Image> _game1Stars;
    public float _attention;
    public float _concentration;
    public int starLevel_Game1 = 0;
    public int _allSteps = 0;
    public GameObject _completeGame1;

    private void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                if (!EventSystem.current.IsPointerOverGameObject(touch.fingerId))
                {
                    if (_overFlowMenu.activeInHierarchy == true)
                    {
                        _overFlowMenu.SetActive(false);
                    }
                }
            }
        }

        //if (Input.GetMouseButtonDown(0))
        //{
        //    print("1");
        //    if (!EventSystem.current.IsPointerOverGameObject())
        //    {
        //        print("2");
        //        if (_overFlowMenu.activeInHierarchy == true)
        //        {
        //            print("3");
        //            _overFlowMenu.SetActive(false);
        //        }
        //    }
        //    else
        //    {
        //        print("4");
        //    }
        //}
    }

    public void OnClickGame_1()
    {
        if (PlayerPrefs.HasKey("Game_1_Over"))
        {
            if(PlayerPrefs.GetString("Game_1_Over") == "True")
            {
                _completeGame1.SetActive(true);
            }
            else
            {
                SoundManager.Instance.PlayButtonClickSound();
                SceneManager.LoadScene(3);
            }
        }
        else
        {
            SoundManager.Instance.PlayButtonClickSound();
            SceneManager.LoadScene(3);
        }
    }

    public void OnClickGame_2()
    {
        SceneManager.LoadScene("EF_Menu");
    }

    public void OnClickGame_3()
    {
        SceneManager.LoadScene("LanguageMenu");
    }

    public void OnClickGame_4()
    {
        SceneManager.LoadScene("MemoryHomePage");
    }

    public void OnClick_OverFlowMenu()
    {
        SoundManager.Instance.PlayButtonClickSound();
        _overFlowMenu.SetActive(true);
    }

    public void OnClickProfile()
    {
        SoundManager.Instance.PlayButtonClickSound();
        _menuTitle.text = "Profile";
        _menuAnim.SetBool("Show", true);
        _profile.SetActive(true);
        _overFlowMenu.SetActive(false);

        // Set player data
        _userName.text = PlayerPrefs.GetString("UserName");
        _password.text = PlayerPrefs.GetString("Password");
        _patientName.text = PlayerPrefs.GetString("PatientName");
        _patientAge.text = PlayerPrefs.GetString("PatientAge");
        _patientResult.text = PlayerPrefs.GetString("PatientResult");
        _patientMail.text = PlayerPrefs.GetString("PatientMail");
        _careGiverName.text = PlayerPrefs.GetString("CareGiverName");
        _careGiverMail.text = PlayerPrefs.GetString("CareGiverMail");
        _doctorName.text = PlayerPrefs.GetString("DoctorName");
        _doctorMail.text = PlayerPrefs.GetString("DoctorMail");

    }

    public void OnClickBack_Btn()
    {
        SoundManager.Instance.PlayButtonClickSound();
        _menuAnim.SetBool("Show", false);
    }

    public void OnClickExitBtn()
    {
        SoundManager.Instance.PlayButtonClickSound();
        Application.Quit();
    }

    public void OnClickReplayGame_1()
    {
        PlayerPrefs.DeleteKey("Correct_Tries");
        PlayerPrefs.DeleteKey("Incorrect_Tries");
        PlayerPrefs.DeleteKey("Missed_Tries");
        PlayerPrefs.DeleteKey("Vehicle_Speed");
        PlayerPrefs.DeleteKey("Vehicle_Count");
        PlayerPrefs.DeleteKey("_stepNumber");
        PlayerPrefs.DeleteKey("Level_Number");
        PlayerPrefs.DeleteKey("Game_1_Over");
        PlayerPrefs.DeleteKey("IsFirstTime");

        PlayerPrefs.DeleteKey("AttentionWeek1");
        PlayerPrefs.DeleteKey("ConcentrationWeek1");
        PlayerPrefs.DeleteKey("AttentionWeek2");
        PlayerPrefs.DeleteKey("ConcentrationWeek2");
        PlayerPrefs.DeleteKey("AttentionWeek3");
        PlayerPrefs.DeleteKey("ConcentrationWeek3");
        PlayerPrefs.DeleteKey("AttentionWeek4");
        PlayerPrefs.DeleteKey("ConcentrationWeek4");

    }

    public void OnClickNo()
    {
        _completeGame1.SetActive(false);
    }
}
