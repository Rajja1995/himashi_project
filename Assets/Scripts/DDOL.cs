﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DDOL : MonoBehaviour
{
    private static bool isCreated = false;
    private void Awake()
    {
        if (!isCreated)
        {
            DontDestroyOnLoad(this);
            isCreated = true;
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    }
}
